var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var tsify = require('tsify');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var paths = {
    pages: ['src/*.html']
};

gulp.task('copyHtml', function() {
    return gulp.src(paths.pages)
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('updateView', ['dev'], function() {
    browserSync.reload();
});

gulp.task('serve', ['dev'], function() {
    browserSync.init({
        server: "./dist"
    });

    gulp.watch(["src/*.html", "src/ts/*.ts", "src/scss/*.scss"]).on('change', function() {
        gulp.start('updateView');
    });
});

gulp.task('dev', ['copyHtml', 'sass'], function() {
    return browserify({
            basedir: '.',
            debug: true,
            entries: ['src/ts/main.ts'],
            cache: {},
            packageCache: {}
        })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist'));
});